<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>MapTool</name>
    <message>
        <location filename="../maptool.py" line="134"/>
        <source>Imposible to find a path between selected lines</source>
        <translation>Impossible de trouver un chemin entre les deux lignes</translation>
    </message>
</context>
<context>
    <name>Plugin</name>
    <message>
        <location filename="../plugin.py" line="40"/>
        <source>Select shortest path</source>
        <translation>Sélectionner le plus court chemin</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="41"/>
        <source>Select upstream</source>
        <translation>Sélectionner l&apos;amont</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="42"/>
        <source>Select downstream</source>
        <translation>Sélectionner l&apos;aval</translation>
    </message>
</context>
</TS>
