from qgis.PyQt.QtCore import QObject, QTranslator, QCoreApplication
from qgis.PyQt.QtWidgets import QToolButton, QMenu
from qgis.PyQt.QtGui import QIcon
from qgis.core import QgsSettings, QgsMapLayer, QgsWkbTypes
from pathlib import Path

from .maptool import MapTool

PLUGIN_DIRECTORY = Path(__file__).parent
ADMITED_LEYER_WKB_TYPES = (
    QgsWkbTypes.LineString,
    QgsWkbTypes.LineStringZ,
    QgsWkbTypes.LineStringM,
    QgsWkbTypes.LineStringZM,
    QgsWkbTypes.LineString25D)

class Plugin(QObject):

    def __init__(self, iface):
        QObject.__init__(self)
        self.__iface = iface
        self.__button = None
        self.__sp = None
        self.__us = None
        self.__ds = None
        self.__maptool = None

        # initialize locale
        locale = QgsSettings().value('locale/userLocale')[0:2]
        locale_path =PLUGIN_DIRECTORY / 'i18n' / f'{locale}.qm'
        print(locale, locale_path.exists())

        if locale_path.exists():
            self.__translator = QTranslator()
            self.__translator.load(str(locale_path))
            QCoreApplication.installTranslator(self.__translator)

    def initGui(self):
        menu = QMenu()
        self.__sp = menu.addAction(QIcon(str(PLUGIN_DIRECTORY / 'shortest_path.svg')), self.tr("Select shortest path"))
        self.__us = menu.addAction(QIcon(str(PLUGIN_DIRECTORY / 'upstream.svg')), self.tr("Select upstream"))
        self.__ds = menu.addAction(QIcon(str(PLUGIN_DIRECTORY / 'downstream.svg')), self.tr("Select downstream"))

        self.__sp.setCheckable(True)
        self.__us.setCheckable(True)
        self.__ds.setCheckable(True)

        self.__sp.triggered.connect(self.select_shortest_path)
        self.__us.triggered.connect(self.select_upstream)
        self.__ds.triggered.connect(self.select_downstream)

        self.__button = QToolButton()
        self.__button.setCheckable(True)
        self.__button.setMenu(menu)
        self.__button.setDefaultAction(self.__sp)
        self.__button.setPopupMode(QToolButton.MenuButtonPopup)
        self.__iface.selectionToolBar().addWidget(self.__button)
        
        self.__iface.layerTreeView().currentLayerChanged.connect(self.layer_changed)

        self.layer_changed(self.__iface.activeLayer())

    def unload(self):
        self.reset()
        self.__button and self.__button.setParent(None)

    def reset(self):
        if self.__maptool:
            self.__iface.mapCanvas().setMapTool(None)
            self.__maptool.deactivate()
            self.__maptool.setParent(None)
            self.__maptool = None

        self.__us.setChecked(False)
        self.__ds.setChecked(False)
        self.__sp.setChecked(False)

    def layer_changed(self, layer):
        if self.__maptool:
            self.select(self.__maptool.mode)

        self.__button.setEnabled(
            layer is not None and layer.type() == QgsMapLayer.VectorLayer and layer.wkbType() in ADMITED_LEYER_WKB_TYPES)
            
    def select(self, mode):
        if mode == MapTool.UPSTREAM:
            action = self.__us
        elif mode == MapTool.DOWSTREAM:
            action = self.__ds
        elif mode == MapTool.SHORTEST_PATH:
            action = self.__sp
        else:
            assert False

        self.reset()

        layer = self.__iface.activeLayer()
        if layer and layer.type() == QgsMapLayer.VectorLayer and layer.wkbType() in ADMITED_LEYER_WKB_TYPES:
            self.__button.setDefaultAction(action)
            self.__maptool = MapTool(self.__iface.mapCanvas(), layer, mode, action, self.__iface)
            self.__iface.mapCanvas().setMapTool(self.__maptool)

    def select_upstream(self):
        self.select(MapTool.UPSTREAM)

    def select_downstream(self):
        self.select(MapTool.DOWSTREAM)

    def select_shortest_path(self):
        self.select(MapTool.SHORTEST_PATH)

