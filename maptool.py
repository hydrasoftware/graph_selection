from qgis.gui import QgsMapTool
from collections import defaultdict
from qgis.analysis import QgsGraphAnalyzer, QgsGraph
from qgis.PyQt.QtCore import Qt
from qgis.core import QgsGeometry, Qgis, QgsRectangle, QgsCoordinateTransform, QgsProject

class MapTool(QgsMapTool):
    UPSTREAM = 'upstream'
    DOWSTREAM = 'downstream'
    SHORTEST_PATH = 'shortest path'

    def __init__(self, canvas, layer, mode, button, iface):
        """mode: str in ('upstream', 'downstream', 'shortest path'"""
        QgsMapTool.__init__(self, canvas)
        self.mode = mode
        self.layer = layer
        self.button = button
        self.selected = []
        self.iface = iface

        if mode == self.UPSTREAM:
            self.graph = defaultdict(list) 
            for f in self.layer.getFeatures():
                line = f.geometry().asPolyline()
                self.graph[line[-1]].append((line[0], f.id()))
        elif mode == self.DOWSTREAM:
            self.graph = defaultdict(list) 
            for f in self.layer.getFeatures():
                line = f.geometry().asPolyline()
                self.graph[line[0]].append((line[-1], f.id()))
        elif mode == self.SHORTEST_PATH:
            g = QgsGraph()
            edge_map = {}
            vertex_map = {}
            for f in self.layer.getFeatures():
                line = f.geometry().asPolyline()
                w = f.geometry().length()
                if line[0] in vertex_map:
                    s = vertex_map[line[0]]
                else:
                    s = g.addVertex(line[0])
                    vertex_map[line[0]] = s
                if line[-1] in vertex_map:
                    e = vertex_map[line[-1]]
                else:
                    e = g.addVertex(line[-1])
                    vertex_map[line[-1]] = e
                idx = g.addEdge(s, e, [w])
                edge_map[idx] = f.id()
                idx = g.addEdge(e, s, [w])
                edge_map[idx] = f.id()

            self.graph = g
            self.edge_map = edge_map
            self.vertex_map = vertex_map
        else:
            assert False

    def activate(self):
        self.button.setChecked(True)
        self.setCursor(Qt.ArrowCursor)
        self.selected = []

    def deactivate(self):
        self.selected = []
        self.button.setChecked(False)

    def canvasReleaseEvent(self, event):
        x = event.pos().x()
        y = event.pos().y()
        p = self.canvas().getCoordinateTransform().toMapCoordinates(x, y)

        tr = QgsCoordinateTransform(self.canvas().mapSettings().destinationCrs(), self.layer.crs(), QgsProject().instance())
        pul = tr.transform(self.canvas().getCoordinateTransform().toMapCoordinates(x-5, y-5))
        plr = tr.transform(self.canvas().getCoordinateTransform().toMapCoordinates(x+5, y+5))
        
        closest = None
        dc = float('Inf')
        for f in self.layer.getFeatures(QgsRectangle(pul.x(), plr.y(), plr.x(), pul.y())):
            d = f.geometry().distance(QgsGeometry.fromPointXY(p))
            if d < dc:
                closest = f.id()
                dc = d

        add_to_selection = bool(event.modifiers() & Qt.ShiftModifier
            or event.modifiers() & Qt.ControlModifier)

        if closest is not None:
            self.selected.append(closest)
            if self.mode in (self.UPSTREAM, self.DOWSTREAM):
                add_to_selection or self.layer.removeSelection()
                self.stream()
            elif self.mode == self.SHORTEST_PATH:
                if len(self.selected) == 2:
                    add_to_selection or self.layer.removeSelection()
                    self.dijkstra()
                else:
                    self.layer.select(self.selected)
            else:
                assert False
        else:
            add_to_selection or self.layer.removeSelection()
            self.selected = []

    def stream(self):
        if self.mode == self.UPSTREAM:
            sn = [(self.layer.getFeature(self.selected[0]).geometry().asPolyline()[0], self.selected[0])]
        elif self.mode == self.DOWSTREAM:
            sn = [(self.layer.getFeature(self.selected[0]).geometry().asPolyline()[-1], self.selected[0])]
        else:
            assert False
        si = {self.selected[0]}
        while True:
            ns = []
            for s, j in sn:
                si.add(j)
                for t, i in self.graph[s]:
                    if i not in si:
                        si.add(i)
                        ns += self.graph[t]
            sn = ns
            if not len(sn):
                break
        self.layer.select(list(si))
        self.selected = []

    def dijkstra(self):
        assert self.mode == self.SHORTEST_PATH 

        begin = self.layer.getFeature(self.selected[0]).geometry().asPolyline()[0]
        end = self.layer.getFeature(self.selected[1]).geometry().asPolyline()[0]

        idxStart = self.vertex_map[begin]
        idxEnd = self.vertex_map[end]
        (tree, costs) = QgsGraphAnalyzer.dijkstra(self.graph, idxStart, 0)
        if tree[idxEnd] == -1:
            self.iface.messageBar().pushMessage("", self.tr("Imposible to find a path between selected lines"), level=Qgis.Info)
            self.selected = []
            return

        si = {self.selected[0], self.selected[1]}
        while idxEnd != idxStart:
            si.add(self.edge_map[tree[idxEnd]])
            idxEnd = self.graph.edge(tree[idxEnd]).fromVertex()

        self.layer.select(list(si))
        self.selected = []


