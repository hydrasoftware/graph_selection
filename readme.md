# Graph Selection


## In a nutshell

Uses graph algorithm to select lines in line layers.

A tool button <img src='tool_button.png' height="30px"></img> is added to le selection toolbar, it is only enabled when a line layer is selected (:warning: not enabled for multiline layers).

Three selection modes are currently supported:
- Select shortest path: select all lines on the shortest path between two consecutively selected lines, the orientation of lines is ignored (two clicks)
- Select downsteam: select all lines downstream of a select line (one click)
- Select upstream: select all lines upstream of a select line (one click)

Note: the Shift and Control keys are taken into account: selections are added if one or the other is pressed.

<img src="screenshot.png" ></img>


## How it work

On tool selection a graph is build for the active layer (:warning: it's build in memory, use with care with big layers).

The upstream and downstream are implemented in pure python. Shortest path uses the [QGIS analysis implementation of dijkstra](https://docs.qgis.org/3.34/en/docs/pyqgis_developer_cookbook/network_analysis.html) and is pretty fast (instantaneous response observed for > 30k lines).

